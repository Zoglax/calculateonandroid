package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class MainActivity extends AppCompatActivity {

    EditText editTextFirstNumber,editTextSecondNumber,editTextResult;
    Button buttonPlus, buttonMinus, buttonMul, buttonDiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextFirstNumber = findViewById(R.id.editTextFirstNumber);
        editTextSecondNumber = findViewById(R.id.editTextSecondNumber);
        editTextResult = findViewById(R.id.editTextResult);

        buttonPlus = findViewById(R.id.buttonPlus);
        buttonPlus.setOnClickListener(buttonPlusClick);

        buttonMinus = findViewById(R.id.buttonMinus);
        buttonMinus.setOnClickListener(buttonMinusClick);


        buttonMul = findViewById(R.id.buttonMul);
        buttonMul.setOnClickListener(buttonMulClick);


        buttonDiv = findViewById(R.id.buttonDiv);
        buttonDiv.setOnClickListener(buttonDivClick);
    }

    View.OnClickListener buttonPlusClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result = firstNumber+secondNumber;

            editTextResult.setText(Double.toString(result));
        }
    };

    View.OnClickListener buttonMinusClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result = firstNumber-secondNumber;

            editTextResult.setText(Double.toString(result));
        }
    };

    View.OnClickListener buttonMulClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result = firstNumber*secondNumber;

            editTextResult.setText(Double.toString(result));
        }
    };

    View.OnClickListener buttonDivClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            double firstNumber = Double.parseDouble(editTextFirstNumber.getText().toString());
            double secondNumber = Double.parseDouble(editTextSecondNumber.getText().toString());

            double result;

            if(secondNumber==0)
            {
                result=0;
            }
            else
            {
                result=firstNumber/secondNumber;
            }

            editTextResult.setText(Double.toString(result));
        }
    };
}
